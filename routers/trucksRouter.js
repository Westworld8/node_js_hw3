const express = require('express');
const router = express.Router();

const {asyncWrapper} = require('../helpers');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {trucksValidation} = require('./middlewares/validationMiddleware');
const {isDriver, isOwner, isFree, findTruck, validateIsTruckExist} = require('./middlewares/trucksMiddleware');
const {getTrucks, createTruck, getTruck, editTruck, deleteTruck, assignTruck} = require('../controllers/trucksController');

router.get(
    '/',
    asyncWrapper(authMiddleware),
    asyncWrapper(isDriver),
    asyncWrapper(getTrucks)
);

router.post(
    '/',
    asyncWrapper(authMiddleware),
    asyncWrapper(isDriver),
    asyncWrapper(trucksValidation),
    asyncWrapper(createTruck)
);

router.get(
    '/:id',
    asyncWrapper(authMiddleware),
    asyncWrapper(isDriver),
    asyncWrapper(findTruck),
    asyncWrapper(validateIsTruckExist),
    asyncWrapper(isOwner),
    asyncWrapper(getTruck)
);

router.put(
    '/:id',
    asyncWrapper(authMiddleware),
    asyncWrapper(isDriver),
    asyncWrapper(findTruck),
    asyncWrapper(validateIsTruckExist),
    asyncWrapper(isOwner),
    asyncWrapper(trucksValidation),
    asyncWrapper(editTruck)
);

router.delete(
    '/:id',
    asyncWrapper(authMiddleware),
    asyncWrapper(isDriver),
    asyncWrapper(findTruck),
    asyncWrapper(validateIsTruckExist),
    asyncWrapper(isOwner),
    asyncWrapper(deleteTruck)
);

router.post(
    '/:id/assign',
    asyncWrapper(authMiddleware),
    asyncWrapper(isDriver),
    asyncWrapper(findTruck),
    asyncWrapper(validateIsTruckExist),
    asyncWrapper(isFree),
    asyncWrapper(assignTruck),
);

module.exports = router;
