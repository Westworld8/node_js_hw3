const express = require('express');
const router = express.Router();

const {asyncWrapper} = require('../helpers');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {findUser, validateIsUserExist, validateChangingPassword} = require('./middlewares/usersMiddleware');
const {getUserInfo, editUserPassword, deleteUser} = require('../controllers/usersController');

router.get(
    '/',
    asyncWrapper(authMiddleware),
    asyncWrapper(findUser),
    asyncWrapper(validateIsUserExist),
    asyncWrapper(getUserInfo)
);

router.patch(
    '/password',
    asyncWrapper(authMiddleware),
    asyncWrapper(findUser),
    asyncWrapper(validateIsUserExist),
    asyncWrapper(validateChangingPassword),
    asyncWrapper(editUserPassword)
);

router.delete(
    '/',
    asyncWrapper(authMiddleware),
    asyncWrapper(findUser),
    asyncWrapper(validateIsUserExist),
    asyncWrapper(deleteUser)
);

module.exports = router;
