const express = require('express');
const router = express.Router();

const {asyncWrapper} = require('../helpers');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {isShipper, findLoad, validateIsLoadExist, isOwnerLoad} = require('./middlewares/loadsMiddleware');
const {isDriver} = require('./middlewares/trucksMiddleware');
const {findUser} = require('./middlewares/usersMiddleware');
const {
    getLoads, 
    createLoad, 
    getActiveLoads, 
    iterateState, 
    getLoad, 
    editLoad, 
    deleteLoad, 
    postLoad, 
    getShippingInfo
} = require('../controllers/loadsController')

router.get(
    '/',
    asyncWrapper(authMiddleware),
    asyncWrapper(findUser),
    asyncWrapper(getLoads)
);

router.post(
    '/',
    asyncWrapper(authMiddleware),
    asyncWrapper(isShipper),
    asyncWrapper(createLoad)
);

router.get(
    '/active',
    asyncWrapper(authMiddleware),
    asyncWrapper(isDriver),
    asyncWrapper(getActiveLoads)  
);

router.patch(
    '/active/state',
    asyncWrapper(authMiddleware),
    asyncWrapper(isDriver),
    asyncWrapper(iterateState)
);

router.get(
    '/:id',
    asyncWrapper(authMiddleware),
    asyncWrapper(findLoad),
    asyncWrapper(validateIsLoadExist),
    asyncWrapper(isOwnerLoad),
    asyncWrapper(getLoad)
);

router.put(
    '/:id',
    asyncWrapper(authMiddleware),
    asyncWrapper(findLoad),
    asyncWrapper(validateIsLoadExist),
    asyncWrapper(isOwnerLoad),
    asyncWrapper(editLoad)
);

router.delete(
    '/:id',
    asyncWrapper(authMiddleware),
    asyncWrapper(findLoad),
    asyncWrapper(validateIsLoadExist),
    asyncWrapper(isOwnerLoad),
    asyncWrapper(deleteLoad)
);

router.post(
    '/:id/post',
    asyncWrapper(authMiddleware),
    asyncWrapper(findLoad),
    asyncWrapper(validateIsLoadExist),
    asyncWrapper(isOwnerLoad),
    asyncWrapper(postLoad)
);

router.get(
    '/:id/shipping_info',
    asyncWrapper(authMiddleware),
    
);

module.exports = router;
