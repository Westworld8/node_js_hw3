const {User} = require('../../models/userModel');
const {Truck} = require('../../models/truckModel');
const {Load} = require('../../models/loadModel');

const isShipper = async (req, res, next) => {
    const {_id} = req.user;
    const user = await User.findOne({_id});
    
    if(user.role !== 'SHIPPER') {
        return res.status(400).json({
            message: 'You are not a shipper'
        });
    }
    
    next();
};

const findLoad = async (req, res, next) => {
    const {id} = req.params;
    const load = await Load.findOne({_id: id});

    req.foundLoad = load;

    next();
};

const validateIsLoadExist = async (req, res, next) => {
    const load = req.foundLoad;

    if(!load) {
        return res.status(400).json({
            message: 'Load not found'
        });
    }

    next();
};

const isOwnerLoad = async (req, res, next) => {
    const {_id} = req.user;
    const load = req.foundLoad;

    if(load.created_by !== _id) {
        return res.status(400).json({
            message: 'You do not have access to load'
        });
    }

    next();
};

module.exports = {isShipper, findLoad, validateIsLoadExist, isOwnerLoad};