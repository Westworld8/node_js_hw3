const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const {User} = require('../../models/userModel');
const JWT_SECRET = process.env.JWT_SECRET;

const findUser = async (req, res, next) => {
    const {email} = req.body;
    const user = await User.findOne({email});

    req.foundUser = user;

    next();
};

const authMiddleware = async (req, res, next) => {
    const [tokenType, token] = req.headers['authorization'].split(' ');

    if(tokenType !== 'JWT') {
        return res.status(401).json({
            message: 'Required JWT token type'
        });
    }

    if(!token) {
        return res.status(401).json({
            message: 'Not authorized'
        });
    }

    req.user = jwt.verify(token, JWT_SECRET);
    next();
};

const validatePassword = async (req, res, next) => {
    const {password} = req.body;
    const user = req.foundUser;

    if(!user || !(await bcrypt.compare(password, user.password))) {
        return res.status(400).json({
            message: `Wrong email or password`
        });
    }

    next();
};

module.exports = {findUser, authMiddleware, validatePassword};
