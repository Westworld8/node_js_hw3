const Joi = require('joi');

const registerValidation = async (req, res, next) => {
    const schema = Joi.object({
        email: Joi.string()
            .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
            .required(),
    
        password: Joi.string()
            .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
            .required(),

        role: Joi.string()
            .valid('SHIPPER', 'DRIVER')
            .required()
    });

    await schema.validateAsync(req.body);
    next();
};

const loginValidation = async (req, res, next) => {
    const schema = Joi.object({
        email: Joi.string()
            .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
            .required(),
    
        password: Joi.string()
            .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
            .required()
    });

    await schema.validateAsync(req.body);
    next();
};

const trucksValidation = async (req, res, next) => {
    const schema = Joi.object({
        created_by: Joi.string(),
        assigned_to: Joi.string(),
        type: Joi.string()
            .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
            .required(),
        status: Joi.string()
            .valid('OL', 'IS')
    });

    await schema.validateAsync(req.body);
    next();
};

module.exports = {registerValidation, loginValidation, trucksValidation};