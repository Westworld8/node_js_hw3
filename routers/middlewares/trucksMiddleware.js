const {User} = require('../../models/userModel');
const {Truck} = require('../../models/truckModel');

const isDriver = async (req, res, next) => {
    const {_id} = req.user;
    const user = await User.findOne({_id});

    if(user.role !== 'DRIVER') {
        return res.status(400).json({
            message: 'You are not a driver'
        });
    }
    
    next();
};

const isOwner = async (req, res, next) => {
    const truck = req.foundTruck;
    const {_id} = req.user;
    
    if(truck.created_by !== _id) {
        return res.status(400).json({
            message: 'You are not an owner'
        });
    }

    next();
};

const isFree = async (req, res, next) => {
    const truck = req.foundTruck;
    
    if(truck.assigned_to) {
        return res.status(400).json({
            message: `Truck already assigned to driver`
        });
    }

    next();
};

const findTruck = async (req, res, next) => {
    const _id = req.params.id;
    const truck = await Truck.findOne({_id});

    req.foundTruck = truck;

    next();
};

const validateIsTruckExist = async (req, res, next) => {
    const truck = req.foundTruck;

    if(!truck) {
        return res.status(400).json({
            message: 'Truck not found'
        });
    }

    next();
};

module.exports = {isDriver, isOwner, isFree, findTruck, validateIsTruckExist};