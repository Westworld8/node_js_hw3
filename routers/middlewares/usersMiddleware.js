const bcrypt = require('bcrypt');

const {User} = require('../../models/userModel');

const findUser = async (req, res, next) => {
    const {_id} = req.user;
    const user = await User.findOne({_id});

    req.foundUser = user;

    next();
};

const validateIsUserExist = async (req, res, next) => {
    const user = req.foundUser;

    if(!user) {
        return res.status(400).json({
            message: 'User not found'
        });
    }

    next();
};

const validateChangingPassword = async (req, res, next) => {
    const user = req.foundUser;
    const {oldPassword, newPassword} = req.body;

    if(!(await bcrypt.compare(oldPassword, user.password))) {
        return res.status(400).json({
            message: `Incorrect old password`
        });
    }

    if(oldPassword === newPassword) {
        return res.status(400).json({
            message: `Old password is equal to new password`
        });
    }

    next();
};

module.exports = {findUser, validateIsUserExist, validateChangingPassword};
