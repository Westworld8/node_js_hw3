const express = require('express');
const router = express.Router();

const {asyncWrapper} = require('../helpers');
const {registerValidation, loginValidation} = require('./middlewares/validationMiddleware');
const {findUser, validatePassword} = require('./middlewares/authMiddleware');
const {registerUser, loginUser} = require('../controllers/authController');

router.post(
    '/register',
    asyncWrapper(registerValidation),
    asyncWrapper(registerUser)
);

router.post(
    '/login', 
    asyncWrapper(loginValidation), 
    asyncWrapper(findUser), 
    asyncWrapper(validatePassword), 
    asyncWrapper(loginUser)
);

module.exports = router;
