require('dotenv').config();

const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');

const app = express();
const port = process.env.PORT || 8080;
const url = process.env.url;

app.use(express.json());
app.use(morgan('tiny'));

const authRouter = require('./routers/authRouter');
const usersRouter = require('./routers/usersRouter');
const trucksRouter = require('./routers/trucksRouter');
const loadsRouter = require('./routers/loadsRouter');

app.use('/api/auth', authRouter);
app.use('/api/users/me', usersRouter);
app.use('/api/trucks', trucksRouter);
app.use('/api/loads', loadsRouter);

app.use((err, req, res, next) => {
  res.status(500).json({
    message: err.message,
  });
});

const serverStart = async () => {
  try {
    await mongoose.connect(url, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });
  } catch(err) {
    console.log(err.message);
  }

  app.listen(port, () => {
    console.log('Server is running');
  });
};
  
serverStart();