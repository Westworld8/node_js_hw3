const {Truck} = require('../models/truckModel');
const {Load} = require('../models/loadModel');

const getLoads = async (req, res) => {
    const {_id} = req.user;
    const user = req.foundUser;

    const loads = user.role === 'SHIPPER' ? await Load.find({created_by: _id}) : await Load.find({assign_to: _id});

    res.status(200).json({
        loads
    });
};

const createLoad = async (req, res) => {
    const {_id} = req.user;
    const {name, payload, pickup_address, delivery_address, dimensions} = req.body;

    const load = new Load({
        created_by: _id,
        name,
        payload,
        pickup_address,
        delivery_address,
        dimensions
    });

    await load.save();

    res.status(200).json({
        message: 'Load created successfully'
    });
};

const getActiveLoads = async (req, res) => {
    const {_id} = req.user;
    const load = await Load.findOne({assign_to: _id, status: 'ASSIGNED'});

    res.status(200).json({
        load
    })
};

const iterateState = async (req, res) => {
    const {_id} = req.user;
    const load = await Load.findOne({assign_to: _id, status: 'ASSIGNED'});

    if(!load) {
        return res.status(400).json({
            message: 'You do not have an active load'
        });
    }

    let newState = '';
    switch(load.state) {
        case 'En route to Pick Up':
            newState = 'Arrived to Pick Up';
        case 'Arrived to Pick Up':
            newState = 'En route to delivery';
        case 'En route to delivery':
            newState = 'Arrived to delivery';    
    }

    await load.update({state: newState});

    if(load.state === 'Arrived to delivery') {
        await load.update({status: 'SHIPPED'});
    }

    res.status(200).json({
        message: `Load state changed to '${newState}'`
    });
};

const getLoad = async (req, res) => {
    const load = req.foundLoad;

    res.status(200).json({
        load
    });
};

const editLoad = async (req, res) => {
    const load = req.foundLoad;
    const {name, payload, pickup_address, delivery_address, dimensions} = req.body;

    await load.update({
        name,
        payload,
        pickup_address,
        delivery_address,
        dimensions
    });

    res.status(200).json({
        message: 'Load details changed successfully'
    });
};

const deleteLoad = async (req, res) => {
    const load = req.foundLoad;

    await load.remove();

    res.status(200).json({
        message: 'Load deleted successfully'
    });
};

const postLoad = async (req, res) => {
    const load = req.foundLoad;
    console.log('dd');
    if(load.status !== 'NEW') {
        return res.status(400).json({
            message: 'You can post only NEW loads'
        });
    }

    res.status(200).json({
        message: 'Load posted successfully',
        driver_found: true
    });
};

const getShippingInfo = async (req, res) => {

};

module.exports = {getLoads, createLoad, getActiveLoads, iterateState, getLoad, editLoad, deleteLoad, postLoad, getShippingInfo};