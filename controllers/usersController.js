const bcrypt = require('bcrypt');

const getUserInfo = async (req, res) => {
    const {_id, email, created_date} = req.foundUser;

    res.status(200).json({
        user: {
            _id,
            email,
            created_date
        }
    });
};

const editUserPassword = async (req, res) => {
    const user = req.foundUser;

    const {newPassword} = req.body;

    await user.update({password: await bcrypt.hash(newPassword, 10)});

    res.status(200).json({
        message: 'Password changed successfully'
    });
};

const deleteUser = async (req, res) => {
    const user = req.foundUser;

    await user.remove();

    res.status(200).json({
        message: 'Profile deleted successfully'
    });
};

module.exports = {getUserInfo, editUserPassword, deleteUser};
