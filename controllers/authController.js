const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const {User} = require('../models/userModel');
const JWT_SECRET = process.env.JWT_SECRET;

const registerUser = async (req, res) => {
    const {email, password, role} = req.body;

    const user = new User({
        email,
        password: await bcrypt.hash(password, 10),
        role
    });

    await user.save();

    res.status(200).json({
        message: 'Profile created successfully'
    });
};

const loginUser = async (req, res) => {
    const {_id, email, role} = req.foundUser;

    const token = jwt.sign({_id, email, role}, JWT_SECRET);

    res.status(200).json({
        jwt_token: token
    });

};

module.exports = {registerUser, loginUser};