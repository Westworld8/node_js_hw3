const {Truck} = require('../models/truckModel');

const getTrucks = async (req, res) => {
    const {_id} = req.user;

    const trucks = await Truck.find({created_by: _id});

    res.status(200).json({
        trucks
    });
}

const createTruck = async (req, res) => {
    const {_id} = req.user;
    const {type} = req.body;

    const truck = new Truck({
        created_by: _id,
        type
    });

    await truck.save();

    res.status(200).json({
        message: 'Truck created successfully'
    });
};

const getTruck = async (req, res) => {
    const {_id, created_by, assigned_to, type, status, created_date} = req.foundTruck;

    res.status(200).json({
        truck: {
            _id,
            created_by,
            assigned_to,
            type,
            status,
            created_date
        }
    });
};

const editTruck = async (req, res) => {
    const truck = req.foundTruck;
    const {type} = req.body;

    await truck.update({type});

    res.status(200).json({
        message: 'Truck details changed successfully'
    });
};

const deleteTruck = async (req, res) => {
    const truck = req.foundTruck;

    await truck.remove();

    res.status(200).json({
        message: 'Truck deleted successfully'
    });
};

const assignTruck = async (req, res) => {
    const truck = req.foundTruck;
    const assigned_to = req.user._id;

    await truck.update({assigned_to});

    res.status(200).json({
        message: 'Truck assigned successfully'
    });
};

module.exports = {getTrucks, createTruck, getTruck, editTruck, deleteTruck, assignTruck}